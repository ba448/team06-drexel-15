//A script that changes the colors of the buttons when it is first opened it up
//It is to compensate for the fact that the popup window refreshes itself after it is closed
//Uses Chrome.Storage API
//Written by Brandon Au


var penButton = document.getElementById("penButton");
var highlighterButton = document.getElementById("highlighterButton");
var eraserButton = document.getElementById("eraserButton");

chrome.storage.local.get(null, function(item){
    penButton.style.backgroundColor = item.penButtonColor;
    highlighterButton.style.backgroundColor = item.highlighterButtonColor;
    eraserButton.style.backgroundColor = item.eraserButtonColor;
});
