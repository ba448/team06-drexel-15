/*
Javascript file that goes along with popup.js
*/

//Variables that store document.getElementById("id") for various divs
var penButton = document.getElementById("penButton"); 
var highlightButton = document.getElementById("highlightButton");
var eraserButton = document.getElementById("eraserButton");

function changeButtonColor(){
	if(this.style.backgroundColor === "rgb(137, 141, 143)")
		this.style.backgroundColor = "white";
	else
		this.style.backgroundColor = "#898D8F";
	
	//If the button is gray, then change it to white when it is clicked (deselection)
	//If the button is white, then change it to gray when it is clicked (selection)
	
}
penButton.addEventListener("click", changeButtonColor);
function Pen(){
	//When the Pen button is clicked, then change the other buttons to white
	highlightButton.style.backgroundColor = "white";
	eraserButton.style.backgroundColor = "white";
	
	//alert("Pen Button"); //Debugging purposes 
	
	chrome.tabs.executeScript(null, {file: "createCanvas.js"}); //Injects the script from createCanvas.js to the chrome tab
	chrome.tabs.executeScript(null, {file: "Utensils/pen.js"}); //Injects script to be able to draw onto the chrome tab
	
}
penButton.addEventListener("click", Pen);

highlightButton.addEventListener("click", changeButtonColor);
function Highlighter(){
	//When the highlighter button is clicked, then change the other buttons to white
	penButton.style.backgroundColor = "white";
	eraserButton.style.backgroundColor = "white";
	//alert("Highlighter Button"); 
}
highlightButton.addEventListener("click", Highlighter);

eraserButton.addEventListener("click", changeButtonColor);
function Eraser(){
	//When the eraser button is clicked, then change the other buttons to white
	penButton.style.backgroundColor = "white";
	highlightButton.style.backgroundColor = "white";
	//alert("Eraser Button");

}
eraserButton.addEventListener("click", Eraser);