//Script thhat adds a canvas to DOM as the first element of the body
//Wrote by Ryan Canavan and Brandon Au

if(document.getElementById("webpageCanvas") == null){
  var canvas = document.createElement('canvas');
  canvas.id = 'webpageCanvas';
  canvas.width = document.body.clientWidth;//Sets it to the width of webpages
  canvas.height = document.body.clientHeight;//Sets it to the height of most webpages
  canvas.style.display = 'block';
  canvas.style.position = 'absolute';
  canvas.style.zIndex = "10000";

  canvas.style.border = "2px solid purple";

  //console.log(canvas.width);
  //console.log(canvas.height);

  document.body.insertBefore(canvas, document.body.firstChild); //Adds canvas before the rest of the elements in the body

var position = {x: 0, y: 0};
var last = { lastX: 0, lastY: 0 };

  function draw(){
    context.beginPath();
    context.moveTo(last.lastX, last.lastY);
    context.lineTo(position.x, position.y);
    context.closePath();
    context.stroke();
  }
  canvas.addEventListener('mousemove', function(e){
    last.lastX = position.x;
    last.lastY = position.y;

    position.x = e.pageX;
    position.y = e.pageY;
  }, false);

  canvas.addEventListener('mousedown', function(e){
    context.beginPath();
    context.moveTo(position.x, position.y);
    canvas.addEventListener("mousemove", draw, false);
  });

  canvas.addEventListener('mouseup', function(){
    //When the mouse button isn't being held, then it removes the event listener
    canvas.removeEventListener('mousemove', draw, false);
  }, false);
}
