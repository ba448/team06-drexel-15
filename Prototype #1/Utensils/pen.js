var canvas = document.getElementById("webpageCanvas");
var context = canvas.getContext('2d');

var position = {x: 0, y: 0};
var last = {lastX: 0, lastY: 0};

canvas.addEventListener('mousemove', function(e){
	//This function gets the position of the mouse

	last.lastX = position.x;
	last.lastY = position.y;

	position.x = e.pageX - this.offsetLeft;
	position.y = e.pageY - this.offsetTop;
}, false);

context.lineWidth = 2;
context.lineJoin = 'round';
context.lineCap = 'round';
context.strokeStyle = 'black';

canvas.addEventListener('mousedown', function(e){
	//When the mouse button is held, it will activate the drawing method 
	context.beginPath();
	context.moveTo(position.x, position.y);

	canvas.addEventListener('mousemove', draw, false); 
});

canvas.addEventListener('mouseup', function(){
	//When the mouse button isn't being held, then it removes the event listener
		canvas.removeEventListener('mousemove', draw, false);
}, false);

function draw(){
	context.beginPath();
	context.moveTo(last.lastX, last.lastY);
	context.lineTo(position.x, position.y);
	context.closePath();
	context.stroke();
}