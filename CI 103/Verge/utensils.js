//A script that manages the context properties of the canvas such as color and stroke widths
//Written by Brandon Au and Ryan Canavan


var canvas = document.getElementById("webpageCanvas");
var context = canvas.getContext('2d');

var currentTool = "";

function main(utensilClicked, colorClicked, sizeClicked){
    if (utensilClicked == 'pen') {
    currentTool = "penTool";
    context.lineWidth = 2;
    context.lineJoin = 'round';
    context.lineCap = 'round';
    context.strokeStyle = 'black';
    context.globalCompositeOperation = 'source-over';
    context.fill();
    return;
  }
  else if(utensilClicked == 'highlighter'){
    currentTool = "highlighterTool";
    context.lineWidth = 20;
    context.lineJoin = 'bevel';
    context.lineCap = 'butt';
    context.strokeStyle = "rgba(13, 213, 252, .4)";
    context.globalCompositeOperation = 'source-over';
    context.fill();
    return;
  }
  else if(utensilClicked == 'eraser'){
    context.lineWidth = 30;
    context.lineJoin = 'bevel';
    context.lineCap = 'butt';
    context.globalCompositeOperation = 'destination-out';
    context.fill();
    return;
  }
  else if (colorClicked == 'red') {
      if (currentTool == "penTool") {
          context.strokeStyle = 'red';
      }
      else if (currentTool == "highlighterTool") {
          context.strokeStyle = "rgba(13, 213, 252, .4)";
      }
      return;
  }
  else if (colorClicked == 'blue') {
      if (currentTool == "penTool") {
          context.strokeStyle = 'blue';
      }
      else if (currentTool == "highlighterTool") {
          context.strokeStyle = "rgba(13, 213, 252, .4)";
      }
      return;
  }
  else if (colorClicked == 'black') {
      if (currentTool == "penTool") {
          context.strokeStyle = 'black';
      }
      else if (currentTool == "highlighterTool") {
          context.strokeStyle = "rgba(13, 213, 252, .4)";
      }
      return;
  }
  else if (sizeClicked == 'small') {
      if (currentTool == "penTool") {
          context.lineWidth = 2;
      }
      else if (currentTool == "highlighterTool") {
          context.lineWidth = 20;
      }
      return;
  }
  else if (sizeClicked == 'medium') {
      if (currentTool == "penTool") {
          context.lineWidth = 4;
      }
      else if (currentTool == "highlighterTool") {
          context.lineWidth = 20;
      }
      return;
  }
  else if (sizeClicked == 'large') {
      if (currentTool == "penTool") {
          context.lineWidth = 6;
      }
      else if (currentTool == "highlighterTool") {
          context.lineWidth = 20;
      }
      return;
  }
}


chrome.runtime.onMessage.addListener(function(request, sender){
  main(request.button, request.colorButton, request.sizeButton);
});
