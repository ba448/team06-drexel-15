//A script that changes the color of the buttons when the popup window is the one being focused
//Uses Chrome.Storage API
//Written by Brandon Au

var penButton = document.getElementById("penButton");
var highlighterButton = document.getElementById("highlightButton");
var eraserButton = document.getElementById("eraserButton");

function addScripts(){
  //Add the createCanvas.js and utensils.js files when the buttons are gray
  chrome.tabs.executeScript(null, {file: "createCanvas.js"});
  chrome.tabs.executeScript(null, {file: "utensils.js"});
}

document.getElementById("eventDelegate").addEventListener("click", function(e){
  var target = e.target;

  //If the button is gray, change the button color to white
  //If the button is white, make the button clicked gray and the rest white

  if(target.id == "penButton" || target.id == "penPic" || target.id == "penName"){
    if(penButton.style.backgroundColor === "rgb(137, 141, 143)"){
      penButton.style.backgroundColor = "white";
    }
    else {
      addScripts();
      penButton.style.backgroundColor = "rgb(137, 141, 143)";
      highlighterButton.style.backgroundColor = "white";
      eraserButton.style.backgroundColor = "white";
    }
  }
  else if(target.id == "highlightButton" || target.id == "highlighterName" || target.id == "highlighterPic"){
    if(highlighterButton.style.backgroundColor === "rgb(137, 141, 143)"){
      highlighterButton.style.backgroundColor = "white";

    }
    else{
      addScripts();
      highlighterButton.style.backgroundColor = "rgb(137, 141, 143)";
      penButton.style.backgroundColor = "white";
      eraserButton.style.backgroundColor = "white";
    }
  }
  else if(target.id == "eraserButton" || target.id == "eraserName" || target.id == "eraserPic"){
    if(eraserButton.style.backgroundColor === "rgb(137, 141, 143)"){
      eraserButton.style.backgroundColor = "white";
    }
    else{
      addScripts();
      eraserButton.style.backgroundColor = "rgb(137, 141, 143)";
      highlighterButton.style.backgroundColor = "white";
      penButton.style.backgroundColor = "white";
    }
  }

//Saves the button colors to be loaded when the popup is clicked again
  chrome.storage.local.set({'penButtonColor': penButton.style.backgroundColor}, function(){});
  chrome.storage.local.set({'highlighterButtonColor': highlighterButton.style.backgroundColor}, function(){});
  chrome.storage.local.set({'eraserButtonColor': eraserButton.style.backgroundColor}, function(){});

});
