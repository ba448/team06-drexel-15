//A script that sends messages to content scripts to perform a certain action or to change a property
//Uses Chrome.tabs API
//Written by Brandon Au and Ryan Canavan

var eventDelegator = document.getElementById("eventDelegate");

eventDelegator.addEventListener("click", function(e){
  var target = e.target;

if(target.id == "penButton" || target.id == "penPic" || target.id == "penName"){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
      chrome.tabs.sendMessage(tabs[0].id, {button: 'pen'});
    }
  );
}
else if(target.id == "highlightButton" || target.id == "highlighterName" || target.id == "highlighterPic"){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
    chrome.tabs.sendMessage(tabs[0].id, {button: 'highlighter'});
  }
);
}
else if(target.id == "eraserButton" || target.id == "eraserName" || target.id == "eraserPic"){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
      chrome.tabs.sendMessage(tabs[0].id, {button: 'eraser'});
    }
  );
}
else if (target.id == "redButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { colorButton: 'red' });
    }
  );
}
else if (target.id == "blueButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { colorButton: 'blue' });
    }
  );
}
else if (target.id == "blackButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { colorButton: 'black' });
    }
  );
}
else if (target.id == "smallButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { sizeButton: 'small' });
    }
  );
}
else if (target.id == "mediumButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { sizeButton: 'medium' });
    }
  );
}
else if (target.id == "largeButton") {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { sizeButton    : 'large' });
    }
  );
}

});

document.addEventListener('DOMContentLoaded', function smallCircle() {
    var small = document.getElementById('smallButton');
    small.addEventListener('click', function changeSmallCircle() {
        var image = document.getElementById('smallButton');
        if (image.src.match("clicked")) {
            image.src = "Images/BlackCircle.png";
        } else {
            image.src = "Images/BlackCircleclicked.png";
            document.getElementById('mediumButton').src = "Images/BlackCircle.png";
            document.getElementById('largeButton').src = "Images/BlackCircle.png";
        }
    });
});

document.addEventListener('DOMContentLoaded', function mediumCircle() {
    var medium = document.getElementById('mediumButton');
    medium.addEventListener('click', function changeMediumCircle() {
        var image = document.getElementById('mediumButton');
        if (image.src.match("clicked")) {
            image.src = "Images/BlackCircle.png";
        } else {
            image.src = "Images/BlackCircleclicked.png";
            document.getElementById('smallButton').src = "Images/BlackCircle.png";
            document.getElementById('largeButton').src = "Images/BlackCircle.png";
        }
    });
});

document.addEventListener('DOMContentLoaded', function bigCircle() {
    var big = document.getElementById('largeButton');
    big.addEventListener('click', function changeBigCircle() {
        var image = document.getElementById('largeButton');
        if (image.src.match("clicked")) {
            image.src = "Images/BlackCircle.png";
        } else {
            image.src = "Images/BlackCircleclicked.png";
            document.getElementById('smallButton').src = "Images/BlackCircle.png";
            document.getElementById('mediumButton').src = "Images/BlackCircle.png";
        }
    });
});

document.addEventListener('DOMContentLoaded', function redImage() {
    var red = document.getElementById('redButton');
    red.addEventListener('click', function changeRedImage() {
        var image = document.getElementById('redButton');
        if (image.src.match("clicked")) {
            image.src = "Images/RED.png";
        } else {
            image.src = "Images/REDclicked.png";
            document.getElementById('blueButton').src = "Images/BLUE.png";
            document.getElementById('blackButton').src = "Images/BLACK.png";
        }
    });
});

document.addEventListener('DOMContentLoaded', function blueImage() {
    var blue = document.getElementById('blueButton');
    blue.addEventListener('click', function changeBlueImage() {
        var image = document.getElementById('blueButton');
        if (image.src.match("clicked")) {
            image.src = "Images/BLUE.png";
        } else {
            image.src = "Images/BLUEclicked.png";
            document.getElementById('redButton').src = "Images/RED.png";
            document.getElementById('blackButton').src = "Images/BLACK.png";
        }
    });
});

document.addEventListener('DOMContentLoaded', function blackImage() {
    var black = document.getElementById('blackButton');
    black.addEventListener('click', function changeBlackImage() {
        var image = document.getElementById('blackButton');
        if (image.src.match("clicked")) {
            image.src = "Images/BLACK.png";
        } else {
            image.src = "Images/BLACKclicked.png";
            document.getElementById('redButton').src = "Images/RED.png";
            document.getElementById('blueButton').src = "Images/BLUE.png";
        }
    });
});
